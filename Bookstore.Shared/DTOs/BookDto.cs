﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class BookDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Printer { get; set; }
        public KindOfBookDto KindOfBook { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public virtual List<RatingDto> Ratings { get; set; }
        public string PictureName { get; set; }
        public double AverageOfRatings => GetAverageOfRatings(Ratings);
        
        
        private double GetAverageOfRatings(List<RatingDto> listOfRatings){
            if (listOfRatings is null || !listOfRatings.Any())
            {
                return 0;
            }

            return listOfRatings.Average(x => x.Value);
        }
        
    }
}
