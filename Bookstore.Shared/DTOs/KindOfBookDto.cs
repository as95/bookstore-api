﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class KindOfBookDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
