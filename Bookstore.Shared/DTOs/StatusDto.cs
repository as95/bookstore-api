﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class StatusDto
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
    }
}
