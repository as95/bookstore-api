﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class OrderDto
    {
        public int Id { get; set; }
        public List<PurchasedBookDto> PurchasedBooks { get; set; }
        public double TotalPrice { get; set; }
        public StatusDto Status { get; set; }
        public UserDto User { get; set; }
    }
}
