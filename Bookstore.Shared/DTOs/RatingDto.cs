﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class RatingDto
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
    }
}
