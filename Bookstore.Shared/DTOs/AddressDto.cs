﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class AddressDto
    {
        public string Id { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
    }
}
