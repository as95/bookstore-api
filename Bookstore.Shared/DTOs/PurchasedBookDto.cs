﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.DTOs
{
    public class PurchasedBookDto
    {
        public int Id { get; set; }
        public BookDto Book { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
    }
}
