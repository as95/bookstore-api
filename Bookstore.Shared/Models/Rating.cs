﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class Rating : Entity
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Description { get; set; }
        public virtual Book Book { get; set; }
        public virtual User User { get; set; }
    }
}
