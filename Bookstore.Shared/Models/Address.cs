﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class Address : Entity
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
