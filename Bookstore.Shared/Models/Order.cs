﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class Order : Entity
    {
        public int Id { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<PurchasedBook> PurchasedBooks { get; set; }
        public double TotalPrice { get; set; }
        public virtual Status Status { get; set; } 
    }
}
