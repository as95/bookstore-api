﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class PurchasedBook : Entity
    {
        public int BookId { get; set; }
        public virtual Book Book { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public virtual Order Order { get; set; }
        public int OrderId { get; set; }
    }
}
