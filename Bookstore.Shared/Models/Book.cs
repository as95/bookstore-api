﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class Book : Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public string Printer { get; set; }
        public virtual KindOfBook KindOfBook { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public string PictureName { get; set; }
        public virtual ICollection<Rating> Ratings { get; set; }
    }
}
