﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class Entity: EntityBase
    {
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
