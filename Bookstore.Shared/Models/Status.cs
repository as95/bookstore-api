﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class Status: EntityBase
    {
        public int Id { get; set; }
        public string StatusName { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
    }
}
