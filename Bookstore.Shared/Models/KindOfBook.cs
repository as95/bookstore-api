﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.Models
{
    public class KindOfBook : Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Book> Books { get; set; }
    }
}
