﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class UpdateKindOfBookBindingModel
    {
        public string Name { get; set; }
    }
}
