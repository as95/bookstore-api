﻿using Bookstore.Shared.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class AddOrderBindingModel
    {
        public List<OrderBindingModel> CartItems { get; set; }
    }
}
