﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class AddBookBindingModel
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public string Printer { get; set; }
        public string KindOfBookName { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public IFormFile PictureBook { get; set; }
    }
}
