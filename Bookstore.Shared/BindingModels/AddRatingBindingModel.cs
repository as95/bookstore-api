﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class AddRatingBindingModel
    {
        public int Value { get; set; }
        public string Description { get; set; }
    }
}
