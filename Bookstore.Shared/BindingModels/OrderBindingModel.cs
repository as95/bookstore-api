﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class OrderBindingModel
    {
        public int BookId { get; set; }
        public int Quantity { get; set; }
    }
}
