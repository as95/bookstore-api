﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class UpdateAddressBindingModel
    {
        public string Street { get; set; }
        public string Postcode { get; set; }
        public string City { get; set; }
    }
}
