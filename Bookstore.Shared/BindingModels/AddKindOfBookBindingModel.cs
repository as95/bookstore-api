﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels
{
    public class AddKindOfBookBindingModel
    {
        public string Name { get; set; }
    }
}
