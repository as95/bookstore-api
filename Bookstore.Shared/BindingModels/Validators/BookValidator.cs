﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels.Validators
{
    public class BookValidator: AbstractValidator<AddBookBindingModel>
    {
        public BookValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nazwa jest wymagana");
            RuleFor(x => x.Author).NotEmpty().WithMessage("Autor jest wymagany");
            RuleFor(x => x.Printer).NotEmpty().WithMessage("Wydawnictwo jest wymagane");
            RuleFor(x => x.Price).NotEmpty().WithMessage("Cena jest wymagana");
            RuleFor(x => x.KindOfBookName).NotEmpty().WithMessage("Gatunek jest wymagany");
        }
    }
}
