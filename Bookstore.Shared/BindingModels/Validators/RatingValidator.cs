﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels.Validators
{
    public class RatingValidator: AbstractValidator<AddRatingBindingModel>
    {
        public RatingValidator()
        {
            RuleFor(x => x.Value).NotEmpty().WithMessage("Ocena jest wymagana");
        }
    }
}
