﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.Shared.BindingModels.Validators
{
    public class KindOfBookValidator: AbstractValidator<AddKindOfBookBindingModel>
    {
        public KindOfBookValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Nazwa jest wymagana");
        }
    }
}
