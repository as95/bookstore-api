﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.Xml;
using System.Text;
using FluentValidation;

namespace Bookstore.Shared.BindingModels.Validators
{
    public class RegisterValidator: AbstractValidator<RegisterBindingModel>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.Username)
                .NotEmpty().WithMessage("Nazwa użytkownika jest wymagana")
                .MinimumLength(4).WithMessage("Nazwa użytkownika musi mieć więcej niż 4 znaki");
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("Imię jest wymagane");
            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("Nazwisko jest wymagane");
            RuleFor(x => x.Email)
                .NotEmpty().WithMessage("Adres email jest wymagany")
                .EmailAddress().WithMessage("Zły format adresu email");
            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("Hasło jest wymagane")
                .MinimumLength(6).WithMessage("Hasło musi mieć więcej niż 6 znaków");
            RuleFor(x => x.ConfirmPassword)
                .NotEmpty().WithMessage("Potwierdzenie hasła jest wymagane");

            RuleFor(x => x).Custom((x, context) =>
            {
                if (x.Password != x.ConfirmPassword)
                {
                    context.AddFailure(nameof(x.Password), "Passwords should match");
                }
            });
        }
    }
}
