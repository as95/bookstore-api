﻿using System.Linq;
using AutoMapper;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;

namespace Bookstore.BLL.Mapper
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UpdateUserDataBindingModel, User>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            

            CreateMap<Book, BookDto>().ReverseMap();
            CreateMap<AddBookBindingModel, Book>()
                .ForMember(dest => dest.KindOfBook, opt => opt.Ignore());
            CreateMap<UpdateBookBindingModel, Book>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));


            CreateMap<KindOfBook, KindOfBookDto>().ReverseMap();
            CreateMap<AddKindOfBookBindingModel, KindOfBook>().ReverseMap();
            CreateMap<UpdateKindOfBookBindingModel, KindOfBook>()
                .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<AddAddressBindingModel, Address>();
            CreateMap<UpdateAddressBindingModel, Address>();
            CreateMap<Address, AddressDto>();

            CreateMap<Order, OrderDto>();
            CreateMap<PurchasedBook, PurchasedBookDto>();

            CreateMap<Rating, RatingDto>()
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.User.UserName));
            CreateMap<AddRatingBindingModel, Rating>();

            CreateMap<Status, StatusDto>();
        }
    }
}
