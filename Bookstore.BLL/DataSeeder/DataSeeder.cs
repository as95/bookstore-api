﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using Bookstore.DAL.Data;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;

namespace Bookstore.BLL.DataSeeder
{
    public class DataSeeder
    {
        private readonly RoleManager<Role> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly BookstoreDbContext _dbContext;
        private readonly IStatusRepository _statusRepository;

        public DataSeeder(RoleManager<Role> roleManager, UserManager<User> userManager, BookstoreDbContext dbContext, IStatusRepository statusRepository)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _dbContext = dbContext;
            _statusRepository = statusRepository;
        }

        public async Task SeedAdminUser()
        {
            var roles = new []
                {
                    "Administrator",
                    "User"
                }
                .Select(x => new Role
                    {
                        Name = x,
                        NormalizedName = x.ToUpper()
                    });

            if (!_roleManager.Roles.Any())
            {
                foreach (var role in roles)
                {
                    await _roleManager.CreateAsync(role);
                }
            }

            var administrator = new User{FirstName = "Administrator", LastName = "Administrator", UserName = "admin"};
            if (!_userManager.Users.Any())
            {
                await _userManager.CreateAsync(administrator, "zaq12wsx");
                await _userManager.AddToRoleAsync(administrator, "Administrator");
            }
        }

        public async Task SeedStatuses()
        {
            var statuses = new []
            {
                    new Status { StatusName = "Oczekuje na przyjęcie do realizacji" },
                    new Status { StatusName = "Przyjęty do realizacji" },
                    new Status { StatusName = "Zamówienie zrealizowane" }
            };

            if (!_dbContext.Statuses.Any())
            {
                foreach (var status in statuses)
                {
                    await _statusRepository.AddAsync(status);
                }
            }
        }
    }
}
