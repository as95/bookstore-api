﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Identity;

namespace Bookstore.BLL.Services
{
    public class AddressService: IAddressService
    {
        private readonly IAddressRepository _addressRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public AddressService(IAddressRepository addressRepository, UserManager<User> userManager, IMapper mapper)
        {
            _addressRepository = addressRepository;
            _userManager = userManager;
            _mapper = mapper;
        }

        public async Task<ResultDto<AddressDto>> AddAddress(AddAddressBindingModel address, string userId)
        {
            var response = new ResultDto<AddressDto>();

            var user = await _userManager.FindByNameAsync(userId);

            if (user == null)
            {
                response.Errors.Add("Użytkownik nie istnieje");
                return response;
            }

            var addressIsExist = await _addressRepository.ExistsAsync(x => x.User == user);

            if (addressIsExist)
            {
                response.Errors.Add("Użytkownik posiada adres");
                return response;
            }

            var newAddress = _mapper.Map<AddAddressBindingModel, Address>(address);
            newAddress.User = user;
            newAddress.CreationDate = DateTime.Now;
            newAddress.ModifiedDate = DateTime.Now;

            var addingAddress = await _addressRepository.AddAsync(newAddress);

            if (!addingAddress)
            {
                response.Errors.Add("Nie udało się dodać adresu do bazy");
                return response;
            }

            response.SuccessResult = _mapper.Map<AddressDto>(newAddress);
            return response;
        }

        public async Task<ResultDto<AddressDto>> UpdateAddress(int addressId, UpdateAddressBindingModel addressBindingModel)
        {
            var response = new ResultDto<AddressDto>();

            var address = await _addressRepository.GetByAsync(x => x.Id == addressId);
            if (address == null)
            {
                response.Errors.Add("Adres nie istnieje w bazie");
                return response;
            }

            _mapper.Map(addressBindingModel, address);

            var addressUpdateSucceed = await _addressRepository.UpdateAsync(address);
            if (!addressUpdateSucceed)
            {
                response.Errors.Add("Nie udało się zaktualizowac adresu");
                return response;
            }

            response.SuccessResult = _mapper.Map<AddressDto>(address);
            return response;

        }

        public async Task<ResultDto<AddressDto>> DeleteAddress(int addressId)
        {
            var response = new ResultDto<AddressDto>();

            var addressToDelete = await _addressRepository.GetByAsync(x => x.Id == addressId);

            if (addressToDelete == null)
            {
                response.Errors.Add("Adres nie istnieje w bazie");
                return response;
            }

            var deletingAddress = await _addressRepository.DeleteAsync(addressToDelete);

            if (!deletingAddress)
            {
                response.Errors.Add("Nie udało się usunąć adresu z bazy");
                return response;
            }

            return response;
        }
    }
}
