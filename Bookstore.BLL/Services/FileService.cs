﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.Shared.DTOs;
using Microsoft.AspNetCore.Http;

namespace Bookstore.BLL.Services
{
    public class FileService : IFileService
    {

        private readonly List<string> allowedExtensions = new List<string>() { "image/jpeg", "image/png", "image/jpg" };
        public async Task<string> UploadFile(IFormFile file, string directoryPath)
        {
            if (!Directory.Exists(directoryPath)) Directory.CreateDirectory(directoryPath);

            var fileName = Guid.NewGuid() + Path.GetExtension(file.FileName);
            var path = Path.Combine(directoryPath,
                fileName);

            using (var stream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return fileName;
        }

        public ResultDto<string> ValidateFile(IFormFile file)
        {
            var result = new ResultDto<string>();

            if (!allowedExtensions.Contains(file.ContentType.ToLower()))
            {
                result.Errors.Add("Zły format pliku");
                return result;
            }

            if (file.Length > 1048576)
            {
                result.Errors.Add("Zbyt duży rozmiar pliku");
                return result;
            }

            return result;
        }
    }
}
