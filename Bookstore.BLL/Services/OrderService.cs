﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Internal;

namespace Bookstore.BLL.Services
{
    public class OrderService : IOrderService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;
        private readonly IStatusRepository _statusRepository;
        private readonly UserManager<User> _userManager;
        private readonly IAddressRepository _addressRepository;

        public OrderService(IBookRepository bookRepository, IMapper mapper, IOrderRepository orderRepository, UserManager<User> userManager,
            IStatusRepository statusRepository, IAddressRepository addressRepository)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
            _orderRepository = orderRepository;
            _userManager = userManager;
            _statusRepository = statusRepository;
            _addressRepository = addressRepository;
        }
        public async Task<ResultDto<OrderDto>> SubmitOrder(AddOrderBindingModel order, string username)
        {
            var result = new ResultDto<OrderDto>();

            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                result.Errors.Add("Użytkownik nie istnieje w bazie");
                return result;
            }

            user.Address = await _addressRepository.GetByAsync(x => x.User.Id == user.Id);

            var createdOrder = await CreateOrder(order, user);
            if (createdOrder.IsError)
            {
                result.Errors = createdOrder.Errors;
                return result;
            }

            var addOrder = await _orderRepository.AddAsync(createdOrder.SuccessResult);

            if (!addOrder)
            {
                result.Errors.Add("Nie udało się złożyć zamówienia");
                return result;
            }

            //if (user.Email != null)
            //{
            //    _emailService.SendMessage(user.Email, newOrder);
            //}

            result.SuccessResult = _mapper.Map<OrderDto>(createdOrder.SuccessResult);

            return result;
        }

        public async Task<ResultDto<PagedResult<OrderDto>>> GetAllOrders(int page, int pageSize)
        {
            var result = new ResultDto<PagedResult<OrderDto>>();
            var orders = await _orderRepository.GetAllPagedAsync(page, pageSize);
            if (orders == null)
            {
                result.Errors.Add("Nie znaleziono zasobów");
                return result;
            }

            result.SuccessResult = _mapper.Map<PagedResult<OrderDto>>(orders);
            return result;
        }

        public async Task<ResultDto<PagedResult<OrderDto>>> GetOrdersByUsernameAsync(string username, int page, int pageSize)
        {
            var result = new ResultDto<PagedResult<OrderDto>>();
            if (username == null)
            {
                result.Errors.Add("Użytkownik nie istnieje w bazie");
                return result;
            }
            var orders = await _orderRepository.GetAllPagedByAsync(x => x.User.UserName == username, page, pageSize);
            if (orders == null)
            {
                result.Errors.Add("Nie znaleziono zasobów");
                return result;
            }
            result.SuccessResult = _mapper.Map<PagedResult<OrderDto>>(orders);
            return result;
        }

        public async Task<ResultDto<OrderDto>> UpdateOrderStatus(int orderId, int statusId)
        {
            var result = new ResultDto<OrderDto>();

            var order = await _orderRepository.GetByAsync(x => x.Id == orderId);

            if (order == null)
            {
                result.Errors.Add("Zamówienie nie istnieje");
                return result;
            }

            var status = await _statusRepository.GetByAsync(x => x.Id == statusId);

            if (status == null)
            {
                result.Errors.Add("Status nie istnieje");
                return result;
            }

            order.Status = status;

            var updateOrder = await _orderRepository.UpdateAsync(order);
            if (!updateOrder)
            {
                result.Errors.Add("Nie udało się zaktualizować statusu zamówienia");
                return result;
            }

            result.SuccessResult = _mapper.Map<OrderDto>(order);
            return result;
        }


        private async Task<ResultDto<Order>> CreateOrder(AddOrderBindingModel order, User user)
        {
            var result = new ResultDto<Order>();
            var purchasedBooks = new List<PurchasedBook>();
            var totalPrice = 0.0;
            foreach (var book in order.CartItems)
            {
                var bookInRepository = await _bookRepository.GetByAsync(x => x.Id == book.BookId);

                if (bookInRepository == null)
                {
                    result.Errors.Add("Książka nie istnieje w bazie");
                    return result;
                }

                var purchasedBook = new PurchasedBook()
                {
                    Book = bookInRepository,
                    Quantity = book.Quantity,
                    Price = bookInRepository.Price,
                    CreationDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                };

                purchasedBooks.Add(purchasedBook);
                totalPrice += purchasedBook.Price * purchasedBook.Quantity;
            }



            var newOrder = new Order
            {
                CreationDate = DateTime.Now,
                ModifiedDate = DateTime.Now,
                TotalPrice = totalPrice,
                PurchasedBooks = purchasedBooks,
                User = user
            };

            newOrder.Status = await _statusRepository.GetByAsync(x => x.Id == 1);

            result.SuccessResult = newOrder;
            return result;
        }
    }
}
