﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;

namespace Bookstore.BLL.Services
{
    public class KindOfBookService: IKindOfBookService
    {
        private readonly IKindOfBookRepository _kindOfBookRepository;
        private readonly IMapper _mapper;

        public KindOfBookService(IKindOfBookRepository kindOfBookRepository, IMapper mapper)
        {
            _kindOfBookRepository = kindOfBookRepository;
            _mapper = mapper;
        }

        public async Task<ResultDto<KindOfBookDto>> AddKindOfBookAsync(AddKindOfBookBindingModel kindOfBook)
        {
            var result = new ResultDto<KindOfBookDto>();

            var kindOfBookIsExist = await _kindOfBookRepository.ExistsAsync(x => x.Name == kindOfBook.Name);

            if (kindOfBookIsExist)
            {
                result.Errors.Add("Gatunek istnieje w bazie");
                return result;
            }

            var newKindOfBook = _mapper.Map<KindOfBook>(kindOfBook);

            newKindOfBook.CreationDate = DateTime.Now;
            newKindOfBook.ModifiedDate = DateTime.Now;

            var addingKindOfBook = await _kindOfBookRepository.AddAsync(newKindOfBook);

            if (!addingKindOfBook)
            {
                result.Errors.Add("Nie udało się dodać książki do bazy");
                return result;
            }

            return result;
        }

        public async Task<ResultDto<KindOfBookDto>> DeleteKindOfBookAsync(int kindOfBookId)
        {
            var result = new ResultDto<KindOfBookDto>();

            var kindOfBookToDelete = await _kindOfBookRepository.GetByAsync(x => x.Id == kindOfBookId);
            if (kindOfBookToDelete == null)
            {
                result.Errors.Add("Gatunek nie istnieje w bazie");
                return result;
            }

            var deletingKindOfBook = await _kindOfBookRepository.DeleteAsync(kindOfBookToDelete);

            if (!deletingKindOfBook)
            {
                result.Errors.Add("Nie udało się usunąć gatunku z bazy");
                return result;
            }

            return result;
        }

        public async Task<ResultDto<IEnumerable<KindOfBookDto>>> GetAllKindOfBooksAsync()
        {
            var result = new ResultDto<IEnumerable<KindOfBookDto>>();
            var kindOfBooks = await _kindOfBookRepository.GetAllAsync();

            if (kindOfBooks == null)
            {
                result.Errors.Add("Nie znaleziono zasobu");
                return result;
            }

            result.SuccessResult = _mapper.Map<IEnumerable<KindOfBookDto>>(kindOfBooks);
            return result;
        }

        public async Task<ResultDto<KindOfBookDto>> GetKindOfBookByIdAsync(int kindOfBookId)
        {
            var result = new ResultDto<KindOfBookDto>();
            var kindOfBook = await _kindOfBookRepository.GetByAsync(x => x.Id == kindOfBookId);

            if (kindOfBook == null)
            {
                result.Errors.Add("Nie znaleziono zasobu");
                return result;
            }

            result.SuccessResult = _mapper.Map<KindOfBookDto>(kindOfBook);
            return result;
        }

        public async Task<ResultDto<KindOfBookDto>> UpdateKindOfBookAsync(int kindOfBookId, UpdateKindOfBookBindingModel kindOfBook)
        {
            var result = new ResultDto<KindOfBookDto>();

            var kindOfBookToUpdate = await _kindOfBookRepository.GetByAsync(x => x.Id == kindOfBookId);

            if (kindOfBookToUpdate == null)
            {
                result.Errors.Add("Gatunek nie istnieje w bazie");
                return result;
            }

            kindOfBookToUpdate.Name = kindOfBook.Name;

            var updateKindOfBook = await _kindOfBookRepository.UpdateAsync(kindOfBookToUpdate);

            if (!updateKindOfBook)
            {
                result.Errors.Add("Nie udało się zaktualizować gatunku z bazy");
                return result;
            }

            result.SuccessResult = _mapper.Map<KindOfBookDto>(kindOfBookToUpdate);
            return result;
        }
    }
}
