﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IAddressService
    {
        Task<ResultDto<AddressDto>> AddAddress(AddAddressBindingModel address, string userId);
        Task<ResultDto<AddressDto>> UpdateAddress(int addressId, UpdateAddressBindingModel address);
        Task<ResultDto<AddressDto>> DeleteAddress(int addressId);
    }
}
