﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IRatingService
    {
        Task<ResultDto<RatingDto>> AddRating(string username, int bookId, AddRatingBindingModel ratingModel);
        Task<ResultDto<RatingDto>> DeleteRating(int ratingId, string username);
        Task<ResultDto<PagedResult<RatingDto>>> GetRatingsByBookId(int bookId, int page, int pageSize);
    }
}
