﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IStatusService
    {
        Task<ResultDto<IEnumerable<StatusDto>>> GetStatuses();
    }
}
