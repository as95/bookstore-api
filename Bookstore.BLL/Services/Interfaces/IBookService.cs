﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IBookService
    {
        Task<ResultDto<PagedResult<BookDto>>> GetAllBooksAsync(int page, int pageSize);
        Task<ResultDto<BookDto>> GetBookByIdAsync(int bookId);
        Task<ResultDto<BookDto>> AddBookAsync(AddBookBindingModel book);
        Task<ResultDto<string>> DeleteBookAsync(int bookId);
        Task<ResultDto<BookDto>> UpdateBookAsync(UpdateBookBindingModel bookBindingModel, int bookid);
    }
}
