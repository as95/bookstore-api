﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IAccountService
    {
        Task<ResultDto<UserDto>> Register(RegisterBindingModel registerModel);
        Task<ResultDto<UserDto>> Login(LoginBindingModel loginModel);
        Task<string> LogOut();
        Task<ResultDto<string>> ChangePassword(ChangePasswordBindingModel changePasswordModel, string username);
        Task<ResultDto<UserDto>> GetUserData(string username);
        Task<ResultDto<UserDto>> UpdateUserData(UpdateUserDataBindingModel userDataBindingModel, string username);
    }
}
