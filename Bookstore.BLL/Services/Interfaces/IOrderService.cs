﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IOrderService
    {
        Task<ResultDto<OrderDto>> SubmitOrder(AddOrderBindingModel order, string username);
        Task<ResultDto<PagedResult<OrderDto>>> GetAllOrders(int page, int pageSize);
        Task<ResultDto<PagedResult<OrderDto>>> GetOrdersByUsernameAsync(string username, int page, int pageSize);

        Task<ResultDto<OrderDto>> UpdateOrderStatus(int orderId, int statusId);
    }
}
