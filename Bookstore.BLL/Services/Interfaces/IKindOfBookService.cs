﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IKindOfBookService
    {
        Task<ResultDto<IEnumerable<KindOfBookDto>>> GetAllKindOfBooksAsync();
        Task<ResultDto<KindOfBookDto>> GetKindOfBookByIdAsync(int kindOfBookId);
        Task<ResultDto<KindOfBookDto>> AddKindOfBookAsync(AddKindOfBookBindingModel kindOfBook);
        Task<ResultDto<KindOfBookDto>> DeleteKindOfBookAsync(int kindOfBookId);
        Task<ResultDto<KindOfBookDto>> UpdateKindOfBookAsync(int kindOfBookId, UpdateKindOfBookBindingModel kindOfBook);
    }
}
