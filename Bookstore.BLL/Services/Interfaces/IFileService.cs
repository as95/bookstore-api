﻿using System.Threading.Tasks;
using Bookstore.Shared.DTOs;
using Microsoft.AspNetCore.Http;

namespace Bookstore.BLL.Services.Interfaces
{
    public interface IFileService
    {
        ResultDto<string> ValidateFile(IFormFile file);
        Task<string> UploadFile(IFormFile file, string directoryPath);
    }
}
