﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Identity;

namespace Bookstore.BLL.Services
{
    public class RatingService : IRatingService
    {
        private readonly IRatingRepository _ratingRepository;
        private readonly IBookRepository _bookRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public RatingService(IRatingRepository ratingRepository, IBookRepository bookRepository, UserManager<User> userManager, IMapper mapper)
        {
            _ratingRepository = ratingRepository;
            _bookRepository = bookRepository;
            _userManager = userManager;
            _mapper = mapper;
        }
        public async Task<ResultDto<RatingDto>> AddRating(string username, int bookId, AddRatingBindingModel ratingModel)
        {
            var result = new ResultDto<RatingDto>();

            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                result.Errors.Add("Nie ma takiego użytkownika");
                return result;
            }

            var book = await _bookRepository.GetByAsync(x => x.Id == bookId);
            if (book == null)
            {
                result.Errors.Add("Nie ma takiej ksiązki");
                return result;
            }

            var ratingisExist = await _ratingRepository.ExistsAsync(x => x.Book == book && x.User == user);
            if (ratingisExist)
            {
                result.Errors.Add("Dodałeś już ocenę do tej książki");
                return result;
            }

            var rating = _mapper.Map<Rating>(ratingModel);
            rating.Book = book;
            rating.User = user;
            rating.CreationDate = DateTime.Now;
            rating.ModifiedDate = DateTime.Now;

            var addRating = await _ratingRepository.AddAsync(rating);
            if (!addRating)
            {
                result.Errors.Add("Nie udało sie dodać oceny");
                return result;
            }

            result.SuccessResult = _mapper.Map<RatingDto>(rating);
            return result;
        }

        public async Task<ResultDto<RatingDto>> DeleteRating(int ratingId, string username)
        {
            var result = new ResultDto<RatingDto>();

            var rating = await _ratingRepository.GetByAsync(x => x.Id == ratingId);
            if (rating == null)
            {
                result.Errors.Add("Nie znaleziono oceny");
                return result;
            }

            var user = await _userManager.FindByNameAsync(username);

            var permissionToDelete = rating.User == user || await _userManager.IsInRoleAsync(user, "Administrator");

            if (!permissionToDelete)
            {
                result.Errors.Add("Nie masz uprawnień, by usunąć tą ocenę");
                return result;
            }

            var deleteRating = await _ratingRepository.DeleteAsync(rating);
            if (!deleteRating)
            {
                result.Errors.Add("Nie udało się usunąć oceny");
                return result;
            }

            result.SuccessResult = _mapper.Map<RatingDto>(rating);
            return result;
        }

        public async Task<ResultDto<PagedResult<RatingDto>>> GetRatingsByBookId(int bookId, int page, int pageSize)
        {
            var result = new ResultDto<PagedResult<RatingDto>>();
            var ratings = await _ratingRepository.GetAllPagedByAsync(x => x.Book.Id == bookId, page, pageSize);

            if (ratings == null)
            {
                result.Errors.Add("Nie znaleziono zasobu");
                return null;
            }

            result.SuccessResult = _mapper.Map<PagedResult<RatingDto>>(ratings);
            return result;
        }
    }
}
