﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Identity;

namespace Bookstore.BLL.Services
{
    public class AccountService: IAccountService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly IMapper _mapper;
        private readonly IAddressRepository _addressRepository;

        public AccountService(UserManager<User> userManager, SignInManager<User> signInManager, IMapper mapper, IAddressRepository addressRepository)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _addressRepository = addressRepository;
        }
        public async Task<ResultDto<UserDto>> Register(RegisterBindingModel registerModel)
        {
            var result = new ResultDto<UserDto>();
            var userValidation = await ValidateUser(registerModel.Username, registerModel.Email);
            if (userValidation.IsError)
            {
                result.Errors = userValidation.Errors;
                return result;
            }

            var user = new User()
            {
                UserName = registerModel.Username,
                FirstName = registerModel.FirstName,
                LastName = registerModel.LastName,
                Email = registerModel.Email,
            };

            var createUser = await _userManager.CreateAsync(user, registerModel.Password);

            if (!createUser.Succeeded)
            {
                result.Errors.Add("Nie udało się stworzyć uzytkownika");
                return result;
            }

            var addUserToRole = await _userManager.AddToRoleAsync(user, "User");

            if (!addUserToRole.Succeeded)
            {
                result.Errors.Add("Nie udało przypisać roli do użytkownika");
                return result;
            }

            return result;
        }

        public async Task<ResultDto<UserDto>> Login(LoginBindingModel loginModel)
        {
            var result = new ResultDto<UserDto>();
            var user = await _userManager.FindByNameAsync(loginModel.Username);

            if (user == null)
            {
                result.Errors.Add("Błędne dane logowania");
                return result;
            }

            var validatePassword = await _userManager.CheckPasswordAsync(user, loginModel.Password);
            if (!validatePassword)
            {
                result.Errors.Add("Błędne dane logowania");
                return result;
            }

            var login = await _signInManager.PasswordSignInAsync(user, loginModel.Password, true, false);
            if (!login.Succeeded)
            {
                result.Errors.Add("Nie udało się zalogować");
                return result;
            }

            result.SuccessResult = _mapper.Map<UserDto>(user);
            return result;
        }

        public async Task<string> LogOut()
        {
            await _signInManager.SignOutAsync();
            return "Pomyślnie wylogowano";
        }

        public async Task<ResultDto<UserDto>> GetUserData(string username)
        {
            var result = new ResultDto<UserDto>();
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                result.Errors.Add("Użytkownik o podanej nazwie użytkownika istnieje");
                return result;
            }

            user.Address = await _addressRepository.GetByAsync(x => x.User.Id == user.Id);

            result.SuccessResult = _mapper.Map<UserDto>(user);
            return result;
        }

        public async Task<ResultDto<string>> ChangePassword(ChangePasswordBindingModel changePasswordModel, string username)
        {
            var result = new ResultDto<string>();
            var user = await _userManager.FindByNameAsync(username);

            if (user == null)
            {
                result.Errors.Add("Użytkownik o podanej nazwie użytkownika istnieje");
                return result;
            }

            var validateCurrentPassword = await _userManager.CheckPasswordAsync(user, changePasswordModel.CurrentPassword);
            if (!validateCurrentPassword)
            {
                result.Errors.Add("Nieprawidłowe stare hasło");
                return result;
            }

            var changePassword =await _userManager.ChangePasswordAsync(user, changePasswordModel.CurrentPassword, changePasswordModel.NewPassword);
            if (!changePassword.Succeeded)
            {
                result.Errors.Add("Nie udało się zmienić hasła");
                return result;
            }

            await _userManager.UpdateSecurityStampAsync(user);
            return result;
        }

        public async Task<ResultDto<UserDto>> UpdateUserData(UpdateUserDataBindingModel userDataBindingModel, string username)
        {
            var result = new ResultDto<UserDto>();
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                result.Errors.Add("Użytkownik o podanej nazwie użytkownika istnieje");
                return result;
            }

            _mapper.Map(userDataBindingModel, user);

            var updateUser = await _userManager.UpdateAsync(user);
            if (!updateUser.Succeeded)
            {
                result.Errors.Add("Nie udało się zaktualizować danych uzytkownika");
                return result;
            }

            result.SuccessResult = _mapper.Map<UserDto>(user);
            return result;
        }

        private async Task<ResultDto<string>> ValidateUser(string username, string email = null)
        {
            var result = new ResultDto<string>();
            var userByUsername = await _userManager.FindByNameAsync(username);
            if (userByUsername != null)
            {
                result.Errors.Add("Użytkownik o podanej nazwie użytkownika istnieje");
                return result;
            }

            if (email != null)
            {
                var userByEmail = await _userManager.FindByEmailAsync(email);
                if (userByEmail != null)
                {
                    result.Errors.Add("Użytkownik o podanym emailu istnieje");
                    return result;
                }
            }

            return result;
        } 
    }
}
