﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Hosting;

namespace Bookstore.BLL.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;
        private readonly IKindOfBookRepository _kindOfBookRepository;
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _env; 
        private readonly string destPath = Path.Combine(Directory.GetCurrentDirectory(), "pictures\\Books");

        public BookService(IBookRepository bookRepository, IKindOfBookRepository kindOfBookRepository,IFileService fileService, IMapper mapper, IHostingEnvironment env)
        {
            _bookRepository = bookRepository;
            _kindOfBookRepository = kindOfBookRepository;
            _fileService = fileService;
            _mapper = mapper;
            _env = env;
        }

        public async Task<ResultDto<BookDto>> AddBookAsync(AddBookBindingModel book)
        {
            var response = new ResultDto<BookDto>(); 
            var bookIsExist = await _bookRepository.ExistsAsync(x => x.Name == book.Name && x.Author == book.Author);
            if (bookIsExist)
            {
                response.Errors.Add("Książka istnieje w bazie");
                return response;
            }

            var newBook = _mapper.Map<Book>(book);

            if (book.PictureBook != null)
            {
                var fileValidation = _fileService.ValidateFile(book.PictureBook);

                if (fileValidation.IsError)
                {
                    response.Errors = fileValidation.Errors;
                    return response;
                }
                
                newBook.PictureName = await _fileService.UploadFile(book.PictureBook, destPath);
            }

            var kindOfBook = await _kindOfBookRepository.GetByAsync(x => x.Name == book.KindOfBookName);
            if (kindOfBook == null)
            {
                newBook.KindOfBook = new KindOfBook()
                {
                    Name = book.KindOfBookName
                };
            }
            else
            { 
                newBook.KindOfBook = kindOfBook;
            }

            newBook.CreationDate = DateTime.Now;
            newBook.ModifiedDate = DateTime.Now;

            var addingBook = await _bookRepository.AddAsync(newBook);

            if (!addingBook)
            {
                response.Errors.Add("Nie udało się dodać książki do bazy");
                return response;
            }

            response.SuccessResult = _mapper.Map<BookDto>(newBook);
            return response;
        }

        public async Task<ResultDto<PagedResult<BookDto>>> GetAllBooksAsync(int page, int pageSize)
        {
            var result = new ResultDto<PagedResult<BookDto>>();
            var books = await _bookRepository.GetAllPagedAsync(page, pageSize);

            if(books == null)
            {
                result.Errors.Add("Nie znaleziono zasobu");
                return result;
            }

            result.SuccessResult = _mapper.Map<PagedResult<BookDto>>(books);
            return result;
        }

        public async Task<ResultDto<BookDto>> GetBookByIdAsync(int bookId)
        {
            var result = new ResultDto<BookDto>();
            var book = await _bookRepository.GetByAsync(x => x.Id == bookId);

            if (book == null)
            {
                result.Errors.Add("Nie znaleziono zasobu");
                return result;
            }

            result.SuccessResult = _mapper.Map<BookDto>(book);
            return result;
        }

        public async Task<ResultDto<string>> DeleteBookAsync(int bookId)
        {
            var response = new ResultDto<string>();

            var bookToDelete = await _bookRepository.GetByAsync(x => x.Id == bookId);

            if (bookToDelete == null)
            {
                response.Errors.Add("Książka nie istnieje w bazie");
                return response;
            }

            var deletingBook = await _bookRepository.DeleteAsync(bookToDelete);

            if (!deletingBook)
            {
                response.Errors.Add("Nie udało się usunąć książki z bazy");
                return response;
            }

            return response;
        }

        public async Task<ResultDto<BookDto>> UpdateBookAsync(UpdateBookBindingModel bookBindingModel, int bookid)
        {
            var result = new ResultDto<BookDto>();

            var book = await _bookRepository.GetByAsync(x => x.Id == bookid);

            if (book == null)
            {
                result.Errors.Add("Książka nie istnieje w bazie");
                return result;
            }

            _mapper.Map(bookBindingModel, book);

            if (bookBindingModel.KindOfBookName != null)
            {
                var kindOfBook = await _kindOfBookRepository.GetByAsync(x => x.Name == bookBindingModel.KindOfBookName);
                if (kindOfBook == null)
                {
                    book.KindOfBook = new KindOfBook()
                    {
                        Name = bookBindingModel.KindOfBookName
                    };
                }
                else
                {
                    book.KindOfBook = kindOfBook;
                }
            }

            var updateBook = await _bookRepository.UpdateAsync(book);

            if (!updateBook)
            {
                result.Errors.Add("Aktualizacja nie powiodła się");
                return result;
            }

            result.SuccessResult = _mapper.Map<BookDto>(book);
            return result;
        }
    }
}
