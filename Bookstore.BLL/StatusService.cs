﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.DTOs;

namespace Bookstore.BLL
{
    public class StatusService: IStatusService
    {
        private readonly IStatusRepository _statusRepository;
        private IMapper _mapper;

        public StatusService(IStatusRepository statusRepository, IMapper mapper)
        {
            _statusRepository = statusRepository;
            _mapper = mapper;
        }

        public async Task<ResultDto<IEnumerable<StatusDto>>> GetStatuses()
        {
            var result = new ResultDto<IEnumerable<StatusDto>>();

            var statuses = await _statusRepository.GetAllAsync();
            if (statuses == null)
            {
                result.Errors.Add("Nie znaleziono zasobu");
                return result;
            }

            result.SuccessResult = _mapper.Map<IEnumerable<StatusDto>>(statuses);
            return result;
        }
    }
}
