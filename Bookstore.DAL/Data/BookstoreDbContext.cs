﻿using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Bookstore.DAL.Data.Configuration;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Bookstore.DAL.Data
{
    public class BookstoreDbContext: IdentityDbContext<User, Role, int>
    {
        public BookstoreDbContext(DbContextOptions<BookstoreDbContext> options) : base(options) { }

        public DbSet<Book> Books { get; protected set; }
        public DbSet<KindOfBook> KindOfBooks { get; protected set; }
        public DbSet<Rating> Ratings { get; protected set; }
        public DbSet<Order> Orders { get; protected set; }
        public DbSet<Address> Addresses { get; protected set; }
        public DbSet<PurchasedBook> PurchasedBooks { get; protected set; }
        public DbSet<Status> Statuses { get; protected set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ApplyConfiguration(new UserConfiguration());
            builder.ApplyConfiguration(new RoleConfiguration());
            builder.ApplyConfiguration(new UserRolesConfiguration());
            builder.ApplyConfiguration(new UserClaimsConfiguration());
            builder.ApplyConfiguration(new UserLoginsConfiguration());
            builder.ApplyConfiguration(new UserTokensConfiguration());
            builder.ApplyConfiguration(new RoleClaimsConfiguration());
            builder.ApplyConfiguration(new BookConfiguration());
            builder.ApplyConfiguration(new KindOfBookConfiguration());
            builder.ApplyConfiguration(new PurchasedBookConfiguration());
            builder.ApplyConfiguration(new OrderConfiguration());
            builder.ApplyConfiguration(new AddressConfiguration());
        }
    }
}
