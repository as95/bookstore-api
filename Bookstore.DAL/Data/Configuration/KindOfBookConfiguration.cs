﻿using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Data.Configuration
{
    public class KindOfBookConfiguration : IEntityTypeConfiguration<KindOfBook>
    {
        public void Configure(EntityTypeBuilder<KindOfBook> builder)
        {
            builder.ToTable("KindOfBooks");

            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.Books).WithOne(x => x.KindOfBook);
        }
    }
}
