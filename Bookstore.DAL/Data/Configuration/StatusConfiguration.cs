﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bookstore.DAL.Data.Configuration
{
    public class StatusConfiguration: IEntityTypeConfiguration<Status>
    {
        public void Configure(EntityTypeBuilder<Status> builder)
        {
            builder.ToTable("Statuses");
        }
    }
}
