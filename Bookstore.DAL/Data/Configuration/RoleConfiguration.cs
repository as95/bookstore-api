﻿using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Data.Configuration
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Roles");

            builder
                .Property(pr => pr.Name)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(pr => pr.NormalizedName)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(pr => pr.ConcurrencyStamp)
                .HasMaxLength(512);
        }
    }
}
