﻿ using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Data.Configuration
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders");

            builder.HasKey(c => c.Id);
            builder.HasMany(x => x.PurchasedBooks).WithOne(x => x.Order);
            builder.HasOne(x => x.User).WithMany(x => x.Orders);
            builder.HasOne(x => x.Status).WithMany(x => x.Orders);
        }
    }
}
