﻿using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Data.Configuration
{
    public class PurchasedBookConfiguration : IEntityTypeConfiguration<PurchasedBook>
    {
        public void Configure(EntityTypeBuilder<PurchasedBook> builder)
        {
            builder.ToTable("PurchasedBooks");

            builder.HasKey(x => new { x.BookId, x.OrderId });
        }
    }
}
