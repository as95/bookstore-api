﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Data.Configuration
{
    public class UserClaimsConfiguration : IEntityTypeConfiguration<IdentityUserClaim<int>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserClaim<int>> builder)
        {
            builder.ToTable("UserClaims");

            builder
                .Property(pr => pr.ClaimType)
                .HasMaxLength(255);

            builder
                .Property(pr => pr.ClaimValue)
                .HasMaxLength(255);
        }
    }
}
