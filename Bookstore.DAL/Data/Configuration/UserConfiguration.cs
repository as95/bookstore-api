﻿using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Data.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();

            builder
                .Property(pr => pr.FirstName)
                .HasMaxLength(50);

            builder
                .Property(pr => pr.LastName)
                .HasMaxLength(50);

            builder
                .Property(pr => pr.UserName)
                .HasMaxLength(50)
                .IsRequired();

            builder
                .Property(pr => pr.NormalizedUserName)
                .HasMaxLength(50);

            builder
                .Property(pr => pr.PhoneNumber)
                .HasMaxLength(20);

            builder
                .Property(pr => pr.ConcurrencyStamp)
                .HasMaxLength(512);

            builder
                .Property(pr => pr.SecurityStamp)
                .HasMaxLength(512);
        }
    }
}
