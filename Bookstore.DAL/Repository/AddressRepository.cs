﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookstore.DAL.Data;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.Models;

namespace Bookstore.DAL.Repository
{
    public class AddressRepository: RepositoryBase<Address>, IAddressRepository
    {
        public AddressRepository(BookstoreDbContext dbContext) : base(dbContext)
        {
        }
    }
}
