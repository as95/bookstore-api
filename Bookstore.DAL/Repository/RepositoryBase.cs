using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Bookstore.Shared.Models;
using Bookstore.DAL.Data;
using Bookstore.DAL.Repository.Extenions;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Bookstore.DAL.Repository
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        protected BookstoreDbContext _dbContext;
        protected DbSet<T> _db;

        public RepositoryBase(BookstoreDbContext dbContext)
        {
            _dbContext = dbContext;
            _db = dbContext.Set<T>();
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.ToListAsync();
        }

        public virtual async Task<IEnumerable<T>> GetAllByAsync(Expression<Func<T, bool>> getBy)
        {
            return await _db.Where(getBy).ToListAsync();
        }


        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize)
        {
            return await _db.GetPagedAsync(page, pageSize);
        }

        public virtual async Task<PagedResult<T>> GetAllPagedByAsync(Expression<Func<T, bool>> getBy, int page, int pageSize)
        {
            return await _db.Where(getBy).GetPagedAsync(page, pageSize);
        }
        
        public virtual async Task<T> GetByAsync(Expression<Func<T, bool>> getBy)
        {
            return await _db.FirstOrDefaultAsync(getBy);
        }

        public virtual async Task<bool> AddAsync(T entity)
        {
            await _db.AddAsync(entity);
            return await SaveAsync();
        }

        public virtual async Task<bool> UpdateAsync(T entity)
        {
            _db.Update(entity);
            return await SaveAsync();
        }

        public virtual async Task<bool> DeleteAsync(T entity)
        {
            _db.Remove(entity);
            return await SaveAsync();
        }

        public virtual async Task<bool> ExistsAsync(Expression<Func<T, bool>> getBy)
        {
            return await _db.AnyAsync(getBy);
        }
        
        private async Task<bool> SaveAsync()
        {
            return await _dbContext.SaveChangesAsync() >= 0;
        }
    }
}