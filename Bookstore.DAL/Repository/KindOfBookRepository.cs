﻿using Bookstore.DAL.Data;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Bookstore.DAL.Repository
{
    public class KindOfBookRepository : RepositoryBase<KindOfBook>, IKindOfBookRepository
    {
        public KindOfBookRepository(BookstoreDbContext dbContext) : base(dbContext)
        {
        }
        public override async Task<KindOfBook> GetByAsync(Expression<Func<KindOfBook, bool>> getBy)
        {
            var query = _dbContext.Set<KindOfBook>()
               .Where(getBy)
               .Include(x => x.Books);

            return await query.FirstOrDefaultAsync();
        }
    }
}
