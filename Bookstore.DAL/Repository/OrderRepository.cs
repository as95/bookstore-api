﻿using Bookstore.DAL.Data;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Bookstore.DAL.Repository.Extenions;
using Bookstore.Shared.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Bookstore.DAL.Repository
{
    public class OrderRepository : RepositoryBase<Order>, IOrderRepository
    {
        public OrderRepository(BookstoreDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<PagedResult<Order>> GetAllPagedAsync(int page, int pageSize)
        {
            return await _db
                .Include(x => x.User)
                    .ThenInclude(x => x.Address)
                .Include(x => x.PurchasedBooks)
                    .ThenInclude(x => x.Book)
                .GetPagedAsync(page, pageSize);

        }

        public override async Task<PagedResult<Order>> GetAllPagedByAsync(Expression<Func<Order, bool>> getBy, int page, int pageSize)
        {
            return await _db
                .Where(getBy)
                .Include(x => x.PurchasedBooks)
                .ThenInclude(x => x.Book)
                .GetPagedAsync(page, pageSize);
        }
    }
}
