using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Bookstore.Shared.Models;
using Bookstore.DAL.Data;
using Bookstore.DAL.Repository.Extenions;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.DTOs;
using Microsoft.EntityFrameworkCore;

namespace Bookstore.DAL.Repository
{
    public class BookRepository : RepositoryBase<Book>, IBookRepository
    {
        public BookRepository(BookstoreDbContext dbContext) : base(dbContext)
        {
        }

        public override async Task<PagedResult<Book>> GetAllPagedAsync(int page, int pageSize)
        {
            var query = _db
                .Include(x => x.KindOfBook)
                .Include(x => x.Ratings)
                    .ThenInclude(x => x.User)
                .AsQueryable();


            return await query.GetPagedAsync(page, pageSize);
        }

        public override async Task<PagedResult<Book>> GetAllPagedByAsync(Expression<Func<Book, bool>> getBy, int page, int pageSize)
        {
            var query = _db
                .Where(getBy)
                .Include(x => x.KindOfBook)
                .Include(x => x.Ratings)
                    .ThenInclude(x => x.User);

            return await query.GetPagedAsync(page, pageSize);
        }

        public override async Task<Book> GetByAsync(Expression<Func<Book, bool>> getBy)
        {
            var query = _db
                .Where(getBy)
                .Include(x => x.KindOfBook)
                .Include(x => x.Ratings)
                    .ThenInclude(x => x.User);

            return await query.FirstOrDefaultAsync();
        }
    }
}