﻿using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Bookstore.DAL.Data;

namespace Bookstore.DAL.Repository
{
    public class RatingRepository : RepositoryBase<Rating>, IRatingRepository
    {
        public RatingRepository(BookstoreDbContext dbContext) : base(dbContext)
        {
        }
    }
}
