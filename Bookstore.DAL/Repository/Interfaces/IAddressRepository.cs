﻿using System;
using System.Collections.Generic;
using System.Text;
using Bookstore.Shared.Models;

namespace Bookstore.DAL.Repository.Interfaces
{
    public interface IAddressRepository: IRepositoryBase<Address>
    {
    }
}
