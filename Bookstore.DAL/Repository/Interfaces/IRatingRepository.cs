﻿using Bookstore.Shared.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bookstore.DAL.Repository.Interfaces
{
    public interface IRatingRepository: IRepositoryBase<Rating>
    {
    }
}
