using Bookstore.Shared.Models;

namespace Bookstore.DAL.Repository.Interfaces
{
    public interface IBookRepository: IRepositoryBase<Book>
    {
        
    }
}