using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Bookstore.Shared.DTOs;
using Bookstore.Shared.Models;

namespace Bookstore.DAL.Repository.Interfaces
{
    public interface IRepositoryBase<T> where T : EntityBase
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<IEnumerable<T>> GetAllByAsync(Expression<Func<T, bool>> getBy);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize);
        Task<PagedResult<T>> GetAllPagedByAsync(Expression<Func<T, bool>> getBy, int page, int pageSize);
        Task<T> GetByAsync(Expression<Func<T, bool>> getBy);
        Task<bool> AddAsync(T entity);
        Task<bool> UpdateAsync(T entity);
        Task<bool> DeleteAsync(T entity);
        Task<bool> ExistsAsync(Expression<Func<T, bool>> getBy);
    }
}