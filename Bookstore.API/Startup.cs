﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Bookstore.BLL;
using Bookstore.BLL.DataSeeder;
using Bookstore.BLL.Mapper;
using Bookstore.BLL.Services;
using Bookstore.BLL.Services.Interfaces;
using FluentValidation;
using FluentValidation.AspNetCore;
using Bookstore.DAL.Data;
using Bookstore.DAL.Repository;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Bookstore.Shared.BindingModels.Validators;
using Bookstore.Shared.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;

namespace Bookstore.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<BookstoreDbContext>(options =>
                    options
                    .EnableSensitiveDataLogging()
                    .UseSqlServer(
                        Configuration.GetConnectionString("DefaultConnection"),
                            opt => opt.MigrationsAssembly("Bookstore.DAL")));

            services
                .AddIdentity<User, Role>(options => options.Password = new PasswordOptions()
                {
                    RequireDigit = false,
                    RequiredLength = 0,
                    RequireLowercase = false,
                    RequireUppercase = false,
                    RequireNonAlphanumeric = false,
                    RequiredUniqueChars = 0
                })
                .AddUserStore<UserStore<User, Role, BookstoreDbContext, int>>()
                .AddRoleStore<RoleStore<Role, BookstoreDbContext, int>>()
                .AddUserManager<UserManager<User>>()
                .AddRoleManager<RoleManager<Role>>()
                .AddDefaultTokenProviders();

            services
                .Configure<SecurityStampValidatorOptions>(options =>
                    options.ValidationInterval = TimeSpan.FromSeconds(1));

            services.ConfigureApplicationCookie(options =>
            {
                options.Events.OnRedirectToLogin = ctx =>
                {
                    if (ctx.Response.StatusCode == 200)
                    {
                        ctx.Response.StatusCode = 401;
                        return Task.FromResult<object>(null);
                    }
                    return Task.CompletedTask;
                };

                options.Events.OnRedirectToAccessDenied = ctx =>
                {
                    if (ctx.Response.StatusCode == 200)
                    {
                        ctx.Response.StatusCode = 404;
                        return Task.FromResult<object>(null);
                    }
                    return Task.CompletedTask;
                };

                options.Cookie.Domain = null;
                options.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.None;
                options.ExpireTimeSpan = TimeSpan.FromDays(30);
            });

            services.AddTransient(typeof(IBookRepository), typeof(BookRepository));
            services.AddTransient(typeof(IKindOfBookRepository), typeof(KindOfBookRepository));
            services.AddTransient(typeof(IOrderRepository), typeof(OrderRepository));
            services.AddTransient(typeof(IRatingRepository), typeof(RatingRepository));
            services.AddTransient(typeof(IAddressRepository), typeof(AddressRepository));
            services.AddTransient(typeof(IStatusRepository), typeof(StatusRepository));

            services.AddTransient<IBookService, BookService>();
            services.AddTransient<IKindOfBookService, KindOfBookService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<IRatingService, RatingService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IAddressService, AddressService>();
            services.AddTransient<IStatusService, StatusService>();
            services.AddTransient<DataSeeder>();

            services.AddTransient<IValidator<AddBookBindingModel>, BookValidator>();
            services.AddTransient<IValidator<AddKindOfBookBindingModel>, KindOfBookValidator>();
            services.AddTransient<IValidator<AddRatingBindingModel>, RatingValidator>();
            services.AddTransient<IValidator<RegisterBindingModel>, RegisterValidator>();

            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            services.AddCors(options => options.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
                builder.AllowAnyOrigin();
                builder.AllowCredentials();
            }));

            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => {
                    options.SerializerSettings.Formatting = Formatting.Indented;
                })
                .AddFluentValidation(fv => {
                    fv.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Bookstore - API", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, DataSeeder dataSeeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            dataSeeder.SeedAdminUser().Wait();
            dataSeeder.SeedStatuses().Wait();

            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseStaticFiles();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}
