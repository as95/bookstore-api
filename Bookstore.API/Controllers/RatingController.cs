﻿using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.Shared.BindingModels;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.API.Controllers
{
    [Route("api/ratings")]
    [ApiController]
    public class RatingController : ControllerBase
    {
        private readonly IRatingService _ratingService;

        public RatingController(IRatingService ratingService)
        {
            _ratingService = ratingService;
        }

        [HttpGet("{bookId}")]
        public async Task<IActionResult> GetRatingsByBookId(int bookId, int page, int pageSize)
        {
            var ratings = await _ratingService.GetRatingsByBookId(bookId, page, pageSize);

            if (ratings == null)
            {
                return NotFound("Nie znaleziono zasobu");
            }

            return Ok(ratings);
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddRating(int bookId, [FromBody] AddRatingBindingModel rating)
        {
            var user = User.Identity.Name;
            var addRating = await _ratingService.AddRating(user, bookId, rating);

            if (addRating.IsError)
            {
                return BadRequest(addRating);
            }

            return Ok(addRating);
        }

        [HttpDelete("{ratingId}")]
        public async Task<IActionResult> DeleteRating(int ratingId, string username)
        {
            var deleteRating = await _ratingService.DeleteRating(ratingId, username);

            if (deleteRating.IsError)
            {
                return BadRequest(deleteRating);
            }

            return Ok(deleteRating);
        }
    }
}