﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.Shared.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.API.Controllers
{
    [Authorize(Roles = "Administrator, User")]
    [Route("api/accounts")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterBindingModel registerModel)
        {
            var result = await _accountService.Register(registerModel);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginBindingModel loginModel)
        {
            var result = await _accountService.Login(loginModel);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            var result = await _accountService.LogOut();
            return Ok(result);
        }

        [HttpPatch("changepassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordBindingModel changePassword)
        {
            var username = User.Identity.Name;
            var result = await _accountService.ChangePassword(changePassword, username);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpGet("getUserData")]
        public async Task<IActionResult> GetUserData()
        {
            var user = User.Identity.Name;
            var result = await _accountService.GetUserData(user);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet("getUserData/{username}")]
        public async Task<IActionResult> GetUserDataByUsername(string username)
        {
            var result = await _accountService.GetUserData(username);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
    }
}