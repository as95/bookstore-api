﻿using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.Shared.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.API.Controllers
{
    [Authorize(Roles = "Administrator, User")]
    [ApiController]
    [Route("api/books")]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAll(int page, int pageSize)
        {
            var result = await _bookService.GetAllBooksAsync(page, pageSize);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("{bookId}")]
        public async Task<IActionResult> GetById(int bookId)
        {
            var result = await _bookService.GetBookByIdAsync(bookId);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost("add")]
        public async Task<IActionResult> AddBook([FromForm]AddBookBindingModel book)
        {
            var addBook = await _bookService.AddBookAsync(book);

            if (addBook.IsError)
            {
                return BadRequest(addBook);
            }

            return Ok(addBook);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{bookId}")]
        public async Task<IActionResult> DeleteBook(int bookId)
        {
            var deleteBook = await _bookService.DeleteBookAsync(bookId);

            if (deleteBook.IsError)
            {
                return BadRequest(deleteBook);
            }

            return Ok(deleteBook);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPatch("{bookId}")]
        public async Task<IActionResult> UpdateBook([FromForm]UpdateBookBindingModel bookBindingModel, int bookId)
        {
            var updateBook = await _bookService.UpdateBookAsync(bookBindingModel, bookId);

            if (updateBook.IsError)
            {
                return BadRequest(updateBook);
            }

            return Ok(updateBook);
        }
    }
}