﻿using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.Shared.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.API.Controllers
{
    [Authorize(Roles = "Administrator, User")]
    [Route("api/kindOfBooks")]
    [ApiController]
    public class KindOfBookController : ControllerBase
    {
        private readonly IKindOfBookService _kindOfBookService;

        public KindOfBookController(IKindOfBookService kindOfBookService)
        {
            _kindOfBookService = kindOfBookService;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllKindOfBooks()
        {
            var kindOfBooks = await _kindOfBookService.GetAllKindOfBooksAsync();

            if (kindOfBooks == null)
            {
                return NotFound("Nie znaleziono zasobu");
            }

            return Ok(kindOfBooks);
        }

        [HttpGet("{kindOfBookId}")]
        public async Task<IActionResult> GetKindOfBookById(int kindOfBookId)
        {
            var kindOfBooks = await _kindOfBookService.GetKindOfBookByIdAsync(kindOfBookId);

            if (kindOfBooks == null)
            {
                return NotFound("Nie znaleziono zasobu");
            }

            return Ok(kindOfBooks);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost("add")]
        public async Task<IActionResult> AddKindOfBook([FromBody] AddKindOfBookBindingModel kindOfBook)
        {
            var addKindOfBook = await _kindOfBookService.AddKindOfBookAsync(kindOfBook);

            if (addKindOfBook.IsError)
            {
                return BadRequest(addKindOfBook);
            }

            return Ok(addKindOfBook);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut("{kindOfBookId}")]
        public async Task<IActionResult> UpdateKindOfBook(int kindOfBookId, [FromBody] UpdateKindOfBookBindingModel kindOfBook)
        {
            var updateKindOfBook = await _kindOfBookService.UpdateKindOfBookAsync(kindOfBookId, kindOfBook);

            if (updateKindOfBook.IsError)
            {
                return BadRequest(updateKindOfBook);
            }

            return Ok(updateKindOfBook);
        }

        [Authorize(Roles = "Administrator")]
        [HttpDelete("{kindOfBookId}")]
        public async Task<IActionResult> DeleteKindOfBook(int kindOfBookId)
        {
            var deleteKindOfBook = await _kindOfBookService.DeleteKindOfBookAsync(kindOfBookId);

            if (deleteKindOfBook.IsError)
            {
                return BadRequest(deleteKindOfBook);
            }

            return Ok(deleteKindOfBook);
        }
    }
}