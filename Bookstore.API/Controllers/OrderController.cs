﻿using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.Shared.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.API.Controllers
{
    [Authorize(Roles = "Administrator, User")]
    [Route("api/orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost("submitOrder")]
        public async Task<IActionResult> SubmitOrder([FromBody] AddOrderBindingModel order)
        {
            var user = User.Identity.Name;
            var result = await _orderService.SubmitOrder(order, user);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet("getAllOrders")]
        public async Task<IActionResult> GetAllOrders(int page, int pageSize)
        {
            var result = await _orderService.GetAllOrders(page, pageSize);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpGet("getOrders")]
        public async Task<IActionResult> GetOrders(int page, int pageSize)
        {
            var user = User.Identity.Name;
            var result = await _orderService.GetOrdersByUsernameAsync(user, page, pageSize);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet("getOrders/{username}")]
        public async Task<IActionResult> GetOrdersByUsername(string username, int page, int pageSize)
        {
            var result = await _orderService.GetOrdersByUsernameAsync(username, page, pageSize);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPatch("{orderId}")]
        public async Task<IActionResult> UpdateOrderStatus(int orderId, int statusId)
        {
            var result = await _orderService.UpdateOrderStatus(orderId, statusId);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
    }
}