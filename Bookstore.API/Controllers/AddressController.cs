﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bookstore.BLL.Services.Interfaces;
using Bookstore.DAL.Repository.Interfaces;
using Bookstore.Shared.BindingModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bookstore.API.Controllers
{
    [Authorize(Roles = "Administrator, User")]
    [Route("api/addresses")]
    [ApiController]
    public class AddressController : ControllerBase
    {
        private readonly IAddressService _addressService;

        public AddressController(IAddressService addressService)
        {
            _addressService = addressService;
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddAddress(AddAddressBindingModel address)
        {
            var user = User.Identity.Name;
            var result = await _addressService.AddAddress(address, user);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpPatch("{addressId}")]
        public async Task<IActionResult> UpdateAddress(UpdateAddressBindingModel address, int addressId)
        {
            var result = await _addressService.UpdateAddress(addressId, address);

            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }

        [HttpDelete("{addressId}")]
        public async Task<IActionResult> DeleteAddress(int addressId)
        {
            var result = await _addressService.DeleteAddress(addressId);
            if (result.IsError)
            {
                return BadRequest(result);
            }

            return Ok(result);
        }
    }
}